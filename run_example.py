from napari import Viewer
from src.measure_particle_size._function import MyWidget
import napari
from skimage import data
import SimpleITK as sitk

viewer = Viewer()
viewer.window.add_dock_widget(MyWidget(viewer))

# viewer.add_image(data.astronaut(), rgb=True)

image = sitk.ReadImage("/home/k539i/Documents/datasets/original/2021_Gotkowski_HZDR-HIF/3D_converted/images/AFK_M1f.nii.gz")
image = sitk.GetArrayFromImage(image)[:500, :500, :500]
# # offset = 400
# # image = image[offset+100:offset+300, offset+100:offset+300, offset+100:offset+300]
# # sitk.WriteImage(sitk.GetImageFromArray(image), "D:/Datasets/DKFZ/2021_Gotkowski_HZDR-HIF/napari-image.nii.gz")
viewer.add_image(image, rgb=False)
#
# labels = sitk.ReadImage(r"C:\Users\Cookie\Documents\GitKraken\napari-split-blob-labels\labels.nii.gz")
# # labels = sitk.ReadImage("D:/Datasets/DKFZ/2021_Gotkowski_HZDR-HIF/napari-labels.nii.gz")
# labels = sitk.GetArrayFromImage(labels)
# # labels = labels[offset+100:offset+300, offset+100:offset+300, offset+100:offset+300]
# # sitk.WriteImage(sitk.GetImageFromArray(labels), "D:/Datasets/DKFZ/2021_Gotkowski_HZDR-HIF/napari-labels.nii.gz")
# viewer.add_labels(labels)

napari.run()
