from qtpy import QtWidgets
from napari_plugin_engine import napari_hook_implementation
import napari
import numpy as np
from napari.layers.labels._labels_constants import Mode
from enum import Enum
from src.measure_particle_size.utils import dynamic_slicing
from skimage.measure import label as ski_label
from skimage.measure import regionprops

class Marker(Enum):
    FOREGROUND = 'green'
    BACKGROUND = 'red'


class MyWidget(QtWidgets.QWidget):
    def __init__(self, viewer: 'napari.viewer.Viewer') -> None:
        super().__init__()
        self.viewer = viewer
        self.setLayout(QtWidgets.QVBoxLayout())

        self.btn_set_markers = QtWidgets.QPushButton('Set Markers')
        self.btn_set_markers.clicked.connect(self.on_set_markers)
        self.layout().addWidget(self.btn_set_markers)
        self.btn_comp_particle_size = QtWidgets.QPushButton('Compute Particle Size')
        self.btn_comp_particle_size.clicked.connect(self.compute_particle_size)
        self.layout().addWidget(self.btn_comp_particle_size)

        self.btn_reset = QtWidgets.QPushButton('Reset')
        self.btn_reset.clicked.connect(self.reset)
        self.layout().addWidget(self.btn_reset)

        self.initialized = False
        self.set_markers = False
        self.image_layer = None
        self.label_layer = None
        self.marker_layer_name = ".markers <hidden>"
        self.label_layer_name = ".binarized <hidden>"
        self.marker_layer = None
        self.marker_coords = []
        self.marker_types = []
        self.marker_colors = []
        self.marker_image_intensities = []

        self.viewer.window.qt_viewer.layers.model().filterAcceptsRow = self.hide_internal_layers

    def on_set_markers(self):
        if not self.initialized:
            self.init()
        self.set_markers = True

        @self.image_layer.mouse_drag_callbacks.append
        def callback_set_marker(layer, event):
            if self.set_markers:
                data_coordinates = self.image_layer.world_to_data(event.position)
                coords = np.round(data_coordinates).astype(int)
                if event.button == 1:  # Left click
                    self.set_marker(coords, Marker.FOREGROUND)
                    yield
                elif event.button == 2:  # Right click
                    self.set_marker(coords, Marker.BACKGROUND)
                    yield

    def init(self):
        self.initialized = True
        layers = self.viewer.layers
        image_layers, label_layers = [], []
        for layer in layers:
            if isinstance(layer, napari.layers.image.image.Image):
                image_layers.append(layer)
        self.image_layer = image_layers[0]

        active_layer = self.viewer.layers.selection.active
        self.marker_layer = self.viewer.add_points(None, name=self.marker_layer_name)
        self.viewer.layers.selection.active = active_layer

    def set_marker(self, coords, marker_type):
        self.marker_coords.append(coords)
        self.marker_types.append(marker_type)
        self.marker_colors.append(marker_type.value)
        self.marker_image_intensities.append(dynamic_slicing(self.image_layer.data, coords))
        active_layer = self.viewer.layers.selection.active
        self.viewer.layers.remove(self.marker_layer_name)
        self.marker_layer = self.viewer.add_points(np.asarray(self.marker_coords), face_color=np.asarray(self.marker_colors), edge_color=np.asarray(self.marker_colors), size=2, name=self.marker_layer_name)
        self.viewer.layers.selection.active = active_layer

    def compute_particle_size(self):
        if self.set_markers and self.marker_coords:
            self.set_markers = False
            foreground_markers, background_markers = [], []
            for marker_image_intensity, marker_type in zip(self.marker_image_intensities, self.marker_types):
                if marker_type == Marker.FOREGROUND:
                    foreground_markers.append(marker_image_intensity)
                else:
                    background_markers.append(marker_image_intensity)
            foreground_markers = np.asarray(foreground_markers)
            background_markers = np.asarray(background_markers)
            print("foreground_markers: ", foreground_markers)
            print("background_markers: ", background_markers)
            if background_markers.max() < foreground_markers.max():
                background_markers = background_markers[background_markers < foreground_markers.min()]
                print("background_markers: ", background_markers)
                threshold = np.mean([foreground_markers.min(), background_markers.max()])
                print("threshold: ", threshold)
                seg = threshold <= self.image_layer.data
                active_layer = self.viewer.layers.selection.active
                self.label_layer = self.viewer.add_labels(seg, name=self.label_layer_name)
                self.viewer.layers.selection.active = active_layer
                seg = ski_label(seg)
                # feret_diameter_max = []
                equivalent_diameter = []
                for region in regionprops(seg):
                    # feret_diameter_max.append(region.feret_diameter_max)
                    equivalent_diameter.append(region.equivalent_diameter)
                # feret_diameter_max = np.asarray(feret_diameter_max)
                equivalent_diameter = np.asarray(equivalent_diameter)
                # feret_diameter_max_indices = np.argsort(-1*feret_diameter_max)
                # feret_diameter_max = feret_diameter_max[feret_diameter_max_indices][:10]
                equivalent_diameter_indices = np.argsort(-1 * equivalent_diameter)
                equivalent_diameter = equivalent_diameter[equivalent_diameter_indices]
                # print("Mean top 10 of mean_feret_diameter_max: ", mean_feret_diameter_max)
                print("Mean top 100 equivalent_diameter in pixels: ", np.mean(equivalent_diameter[:100]))
                print("Mean top 10 equivalent_diameter in pixels: ", np.mean(equivalent_diameter[:10]))
                print("Remember to convert from pixels to mm: value_mm = value_pixel * spacing")
            else:
                raise RuntimeError("foreground_markers.max() < background_markers.max() not implemented yet.")


            # TODO: Measure either regionprops.feret_diameter_max or regionprops.equivalent_diameter https://www.sympatec.com/en/particle-measurement/glossary/particle-shape/

    def reset(self):  # TODO: Reset is bugged
        print("Reset")
        self.marker_coords = []
        self.marker_types = []
        self.marker_colors = []
        self.marker_image_intensities = []
        self.viewer.layers.remove(self.marker_layer_name)
        self.viewer.layers.remove(self.label_layer_name)
        active_layer = self.viewer.layers.selection.active
        self.marker_layer = self.viewer.add_points(None, name=self.marker_layer_name)
        self.viewer.layers.selection.active = active_layer
        self.label_layer = None
        self.set_markers = False

    def hide_internal_layers(self, row, parent):
        return "<hidden>" not in self.viewer.layers[row].name


@napari_hook_implementation
def napari_experimental_provide_dock_widget():
    return MyWidget


